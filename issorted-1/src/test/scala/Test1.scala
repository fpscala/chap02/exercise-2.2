import org.junit.Test
import org.junit.Assert._
import Sorter.isSorted

class Test1 {
  private def isIntSorted(as: Array[Int]): Boolean = {
    isSorted(as, (a: Int, b: Int) => (a <= b))
  }

  @Test def t1(): Unit = {
    val a1 = Array(1)
    assertEquals(true, isIntSorted(a1))
    val a2 = Array(1, 5)
    assertEquals(true, isIntSorted(a2))
    val a2b = Array(5, 1)
    assertEquals(false, isIntSorted(a2b))
    val a3 = Array(1, 5, 10)
    assertEquals(true, isIntSorted(a3))
    val a3b = Array(5, 1, 10)
    assertEquals(false, isIntSorted(a3b))
    val a3c = Array(1, 10, 5)
    assertEquals(false, isIntSorted(a3c))
  }
}
