import Sorter.isSorted

object Main extends App {
  val aa = Array("a", "ab", "bbb")
  val ii = isSorted(aa, (a: String, b: String) => (a <= b))
  println(ii)
}
